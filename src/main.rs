#![allow(dead_code)]

mod ch10;

fn main() {
    println!("Generics:\n");
    ch10::generics::main();
    println!();
    println!("Traits:\n");
    ch10::traits::main();
    println!();
    ch10::lifetimes::main();
}
