// An instance of ImportantExcerpt can't outlive 'part'.
struct ImportantExcerpt<'a> {
    part: &'a str,
}

pub fn main() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);

    let novel = String::from("Call me Ishmael. Some years ago...");
    let first_sentence = novel.split('.').next().expect("Could not find a '.'");
    let _i = ImportantExcerpt {
        part: first_sentence,
    };
}

// All the variables marked with 'a must live the same amount of time.
// Lifetime annotations don't change the lifetime of a value, they
// just let's the compiler know that all the marked variables
// need to have the same lifetime.
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
