trait Summary {
    // Requires an implementation
    fn summarize_author(&self) -> String;

    // A default implementation is provided
    fn summarize(&self) -> String {
        format!("Read more from {}", self.summarize_author())
    }
}

struct NewsArticle {
    headline: String,
    location: String,
    author: String,
    content: String,
}

impl Summary for NewsArticle {
    fn summarize_author(&self) -> String {
        format!("Author {}", self.author)
    }

    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

struct Tweet {
    username: String,
    content: String,
    reply: bool,
    retweet: bool,
}

impl Summary for Tweet {
    fn summarize_author(&self) -> String {
        format!("@{}", self.username)
    }
}

// Accepts any type that implements Summary
fn notify(item: impl Summary) {
    println!("Breaking news: {}", item.summarize());
}

// Does the same thing as notify, but uses trait bound syntax
fn notify_trait_bound<T: Summary>(item: T) {
    println!("Breaking news: {}", item.summarize());
}

// Forces both items to be of the same type
fn notify_trait_bound_two_of_same_type<T: Summary>(item1: T, item2: T) {
    println!(
        "Thing 1: {}, Thing 2: {}",
        item1.summarize(),
        item2.summarize()
    );
}

// Specifies that the returned type implements Summary
fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("horse_ebooks"),
        content: String::from("Horse book"),
        reply: false,
        retweet: false,
    }
}

pub fn main() {
    let tweet = Tweet {
        username: String::from("elon_musk"),
        content: String::from("I am big dumb"),
        reply: false,
        retweet: false,
    };

    let article = NewsArticle {
        headline: String::from("Things Happened!"),
        location: String::from("Berlin, Germany"),
        author: String::from("Stanley Stanmanson"),
        content: String::from("Believe it or not, things happened today!"),
    };

    println!("1 new tweet: {}", tweet.summarize());
    println!("1 new article: {}", article.summarize());

    notify(tweet);
    notify(article);
}
